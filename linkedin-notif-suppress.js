// ==UserScript==
// @name         Linkedin Notification list cleanup
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       https://github.com/Invertisment/
// @match        https://www.linkedin.com/notifications/
// @icon         https://www.google.com/s2/favicons?domain=linkedin.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function getNotificationCards() {
        return Array.from(document.querySelectorAll(".nt-card"))
    }

    function replaceInCardsByMatch(matchTextsLowercaseSet, pastUnreplaceableTexts, cards) {
        const unreplaceableTexts = new Set(pastUnreplaceableTexts) // deliberately not optimized into mutation of Set
        cards.forEach(card => {
            const cardText = card.textContent.toLowerCase()
            if (matchTextsLowercaseSet.has(cardText) || unreplaceableTexts.has(cardText)) { // Already replaced from before
                return
            }
            for (const text of matchTextsLowercaseSet.values()) {
                if (cardText.includes(text)) { // New match
                    card.textContent = text
                    break
                }
            }
            unreplaceableTexts.add(cardText)
        })
        return unreplaceableTexts
    }

    const textsToReplace = new Set([
        "people are reacting to",
        "you may be a good fit",
        "is attending",
        "graduating",
        "trending",
        "recommended by admins of",
        "new post in",
        "was mentioned in",
        "attended",
        "shared a post",
        "featured by the",
        "recommended",
        "shared a story",
    ])

    var unreplaceableTexts = new Set()

    window.setInterval(() => {
        unreplaceableTexts = replaceInCardsByMatch(
            textsToReplace,
            unreplaceableTexts,
            getNotificationCards())
    }, 500)
})();
