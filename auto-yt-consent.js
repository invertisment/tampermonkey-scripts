// ==UserScript==
// @name         Auto YT consent
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://consent.youtube.com/*
// @icon         https://www.google.com/s2/favicons?domain=youtube.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    console.log("MM Auto YT consent")

    function getButtons() {
        return Array.from(document.querySelectorAll("button"))
    }

    function isCustomizeButton(button) {
        return button.textContent === "Customize";
    }

    function isOffButton(button) {
        return button.textContent === "Off";
    }

    function isConfirmButton(button) {
        return button.textContent === "Confirm";
    }

    function filterClickableButtons(buttons) {
        return buttons.filter(b => isCustomizeButton(b) || isOffButton(b) || isConfirmButton(b));
    }

    window.setInterval(() => {
        const buttons = filterClickableButtons(getButtons())
        buttons.forEach(b => b.click())
    }, 1000)

})();
